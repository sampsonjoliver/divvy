import { injectGlobal } from 'styled-components';
import { createMuiTheme } from 'material-ui/styles';

const theme = createMuiTheme({
  palette: {},
});
export default theme;

// tslint:disable-next-line:no-unused-expression
injectGlobal`
  html, body, #root, #root > div {
    height: 100%;
    margin: 0;
    padding: 0;
  }

  .noselect {
    z-index: 0;
  }
`;
