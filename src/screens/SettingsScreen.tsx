import * as React from 'react';
import { AppToolBar, AppTabBar } from 'components/AppToolBar';
import Button from 'material-ui/Button';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import { AppFrame, ScreenFrame, ContentFrame } from 'screens/Frames';
import { TabViewPager } from 'components/TabViewPager';
import { withTheme, Theme, WithStyles } from 'material-ui/styles';
import { ResourcesPage } from 'components/ResourcesPage';
import { BookingsPage } from 'components/BookingsPage';
import { AuthToolbarComponent } from 'components/AuthToolbarComponent';
import { AppDrawer, DrawerWidth } from 'components/AppDrawer';
import { AppDrawerMenu } from 'components/AppDrawerMenu';
import * as moment from 'moment';

interface SettingsScreenProps {
  theme: Theme;
}

class SettingsScreenComponent extends React.Component<
  SettingsScreenProps,
  { drawerOpen: boolean }
> {
  public state = {
    drawerOpen: false,
  };

  public handleDrawerToggle = () => {
    this.setState({ drawerOpen: !this.state.drawerOpen });
  };

  public handleUpClicked = () => {
    this.handleDrawerToggle();
  };

  public render() {
    return (
      <AppFrame>
        <AppBar position="static">
          <AppToolBar onUpClicked={this.handleUpClicked} title="Settings" upButtonType="menu">
            <AuthToolbarComponent />
          </AppToolBar>
        </AppBar>

        <ScreenFrame>
          <AppDrawer
            open={this.state.drawerOpen}
            onClose={this.handleDrawerToggle}
            onOpen={this.handleDrawerToggle}
          >
            <AppDrawerMenu />
          </AppDrawer>

          <ContentFrame>Das Settings</ContentFrame>
        </ScreenFrame>
      </AppFrame>
    );
  }
}

export const SettingsScreen = withTheme()<{}>(SettingsScreenComponent);
