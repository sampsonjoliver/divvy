import * as React from 'react';
import { withStyles, Theme, WithStyles, StyleRules, StyleRulesCallback } from 'material-ui/styles';
import { StyledComponentProps } from 'material-ui';

const themeStyles: StyleRulesCallback<
  'appFrame' | 'screenContentWithTabs' | 'screenContentWithoutTabs' | 'frameContent'
> = theme => ({
  appFrame: {
    height: '100%',
  },
  screenContentWithTabs: {
    backgroundColor: theme.palette.background.default,
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: 'calc(100% - 104px)',
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 112px)',
    },
  },
  screenContentWithoutTabs: {
    backgroundColor: theme.palette.background.default,
    position: 'relative',
    display: 'flex',
    width: '100%',
    height: 'calc(100% - 56px)',
    [theme.breakpoints.up('sm')]: {
      height: 'calc(100% - 64px)',
    },
  },
  frameContent: {
    backgroundColor: theme.palette.background.default,
    height: '100%',
    width: '100%',
    padding: theme.spacing.unit * 3,
  },
});

const decorate = withStyles(themeStyles);

export const AppFrame = decorate<{}>(props => {
  return <div className={props.classes.appFrame}>{props.children}</div>;
});

interface ContentProps {
  withTabs?: boolean;
  children?: React.ReactNode;
}

export const ScreenFrame = decorate<ContentProps>(props => {
  const styleToApply = props.withTabs
    ? props.classes.screenContentWithTabs
    : props.classes.screenContentWithoutTabs;
  return <div className={styleToApply}>{props.children}</div>;
});

export const ContentFrame = decorate(props => {
  return <div className={props.classes.frameContent}>{props.children}</div>;
});
