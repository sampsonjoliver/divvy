import * as React from 'react';
import { AppToolBar, AppTabBar } from 'components/AppToolBar';
import Button from 'material-ui/Button';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import { AppFrame, ScreenFrame } from 'screens/Frames';
import { TabViewPager } from 'components/TabViewPager';
import { withTheme, Theme, WithStyles } from 'material-ui/styles';
import { ResourcesPage } from 'components/ResourcesPage';
import { BookingsPage } from 'components/BookingsPage';
import { AuthToolbarComponent } from 'components/AuthToolbarComponent';
import { AppDrawer, DrawerWidth } from 'components/AppDrawer';
import { AppDrawerMenu } from 'components/AppDrawerMenu';

interface ResourceScreenProps {
  theme: Theme;
}

class HomeScreenComponent extends React.Component<
  ResourceScreenProps,
  { value: number; drawerOpen: boolean }
> {
  public state = {
    value: 0,
    drawerOpen: false,
  };

  public handleChange = (event: React.ChangeEvent<{}>, value: number) => {
    this.setState({ value });
  };

  public handleChangeIndex = (index: number) => {
    this.setState({ value: index });
  };

  public handleDrawerToggle = () => {
    this.setState({ drawerOpen: !this.state.drawerOpen });
  };

  public handleUpClicked = () => {
    this.handleDrawerToggle();
  };

  public render() {
    return (
      <AppFrame>
        <AppBar position="static">
          <AppToolBar onUpClicked={this.handleUpClicked} title="Resources" upButtonType="menu">
            <AuthToolbarComponent />
          </AppToolBar>
          <AppTabBar value={this.state.value} onChange={this.handleChange}>
            <Tab label="Resources" />
            <Tab label="Bookings" />
          </AppTabBar>
        </AppBar>

        <ScreenFrame withTabs>
          <AppDrawer open={this.state.drawerOpen} onClose={this.handleDrawerToggle} onOpen={this.handleDrawerToggle}>
            <AppDrawerMenu />
          </AppDrawer>

          <TabViewPager
            index={this.state.value}
            onChangeIndex={this.handleChangeIndex}
            theme={this.props.theme}
          >
            <ResourcesPage />
            <BookingsPage />
          </TabViewPager>
        </ScreenFrame>
      </AppFrame>
    );
  }
}

export const HomeScreen = withTheme()<{}>(HomeScreenComponent);
