import * as React from 'react';
import { AppToolBar, AppTabBar } from 'components/AppToolBar';
import Button from 'material-ui/Button';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import { TabViewPager } from 'components/TabViewPager';
import { withTheme, Theme, WithStyles } from 'material-ui/styles';
import { ResourcesPage } from 'components/ResourcesPage';
import { BookingsPage } from 'components/BookingsPage';
import { AuthToolbarComponent } from 'components/AuthToolbarComponent';
import { AppDrawer, DrawerWidth } from 'components/AppDrawer';
import { AppDrawerMenu } from 'components/AppDrawerMenu';
import { AppFrame, ContentFrame } from 'screens/Frames';
import styled from 'styled-components';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';

// tslint:disable-next-line:no-var-requires
const bannerImg = require('assets/banner_kids.jpg');
// tslint:disable-next-line:no-var-requires
const peopleImg = require('assets/img_people.jpg');

const BannerImg = styled.div`
  background-repeat: no-repeat;
  background-position: center center;
  background-attachment: fixed;
  background-image: ${`url(${bannerImg})`};
  height: 500px;
  width: 100%;
  margin-top: 60px;

  background-size: cover;

  animation: fadein 2s;

  @keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
`;

interface LandingScreenProps {
  theme: Theme;
}

const Banner: React.StatelessComponent<{ theme: Theme }> = props => {
  return (
    <BannerImg>
      <Grid
        container
        alignItems="flex-start"
        justify="flex-start"
        direction="column"
        style={{ padding: props.theme.spacing.unit * 8 }}
      >
        <Grid item>
          <Typography variant="display4">Divvy</Typography>
        </Grid>
        <Grid item>
          <Typography variant="display3" style={{ width: '65%' }}>
            Sharing stuff, made simple
          </Typography>
        </Grid>
      </Grid>
    </BannerImg>
  );
};

const ImagePanel: React.StatelessComponent<{ theme: Theme }> = props => {
  return (
    <Grid style={{ padding: props.theme.spacing.unit * 8 }} container justify="space-around">
      <Grid item xs={12} sm={6}>
        <Typography align="center" variant="display2">
          Stuff
        </Typography>
        <Typography align="center" variant="body1">
          Nulla eget consectetur nulla. Pellentesque habitant morbi tristique senectus et netus et
          malesuada fames ac turpis egestas. Morbi vel sem nibh.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Typography align="center" variant="display2">
          <img src={peopleImg} width="100%" />
        </Typography>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Typography align="center" variant="display2">
          <img src={peopleImg} width="100%" />
        </Typography>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Typography align="center" variant="display2">
          Stuff
        </Typography>
        <Typography align="center" variant="body1">
          Nulla eget consectetur nulla. Pellentesque habitant morbi tristique senectus et netus et
          malesuada fames ac turpis egestas. Morbi vel sem nibh.
        </Typography>
      </Grid>
    </Grid>
  );
};

const QuotePanel: React.StatelessComponent<{ theme: Theme }> = props => {
  return (
    <Grid
      container
      alignItems="center"
      justify="flex-start"
      direction="column"
      style={{ padding: props.theme.spacing.unit * 8, background: '#eeeeee' }}
    >
      <Grid item>
        <Typography
          style={{
            fontSize: props.theme.typography.title.fontSize,
            fontStyle: 'italic',
          }}
          variant="caption"
        >
          "Divvy does some really dope shit and makes things be real good ye."
        </Typography>
      </Grid>

      <Grid item>
        <Typography variant="body2">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam semper, dolor non
          consectetur ultrices, mauris elit imperdiet neque, quis sollicitudin elit dui a orci.
          Fusce at malesuada diam. Nam in ante condimentum, gravida nulla eu, fermentum lorem. Ut
          nec lectus eu velit placerat cursus eu sit amet nibh. Donec id malesuada felis. Proin
          dapibus, leo at fermentum feugiat, sapien turpis malesuada libero, in luctus risus enim
          vitae mi. In id gravida metus. Phasellus mi purus, hendrerit vitae blandit et, mattis ac
          massa. Fusce vel felis commodo enim porttitor facilisis blandit a felis. Donec vestibulum
          cursus purus, id consectetur ligula. In pretium elit quis massa rhoncus, eu luctus est
          auctor. Maecenas volutpat dapibus volutpat. Fusce ultrices ligula in consectetur feugiat.
          Nulla eget consectetur nulla. Pellentesque habitant morbi tristique senectus et netus et
          malesuada fames ac turpis egestas. Morbi vel sem nibh.
        </Typography>
      </Grid>
    </Grid>
  );
};

const SignupPanel: React.StatelessComponent<{ theme: Theme }> = props => {
  return (
    <Grid style={{ padding: props.theme.spacing.unit * 8 }} container justify="space-around">
      <Grid item>
        <Typography align="center" variant="display2">
          Stuff
        </Typography>
        <Typography align="center" variant="body1">
          Nulla eget consectetur nulla. Pellentesque habitant morbi tristique senectus et netus et
          malesuada fames ac turpis egestas. Morbi vel sem nibh.
        </Typography>
      </Grid>
    </Grid>
  );
};

const Footer: React.StatelessComponent<{ theme: Theme }> = props => {
  return (
    <Paper
      elevation={5}
      style={{
        padding: props.theme.spacing.unit * 2,
        background: props.theme.palette.primary[500],
        color: props.theme.palette.text.primary,
      }}
    >
      <Typography color="inherit" align="center" variant="body2">
        Footer
      </Typography>
    </Paper>
  );
};

class LandingScreenComponent extends React.Component<LandingScreenProps, {}> {
  public render() {
    const spacing = this.props.theme.spacing.unit;
    return (
      <AppFrame>
        <AppBar position="fixed">
          <AppToolBar title="Divvy" upButtonType="none">
            <AuthToolbarComponent />
          </AppToolBar>
        </AppBar>

        <Paper elevation={5}>
          {' '}
          <Banner theme={this.props.theme} />
        </Paper>

        <Paper elevation={0}>
          <ImagePanel theme={this.props.theme} />

          <QuotePanel theme={this.props.theme} />

          <SignupPanel theme={this.props.theme} />
        </Paper>

        <Footer theme={this.props.theme} />
      </AppFrame>
    );
  }
}

export const LandingScreen = withTheme()<{}>(LandingScreenComponent);
