import * as React from 'react';
import * as firebase from 'wrappers/firebase';
import firebaseui from 'firebaseui';
import 'firebaseui/dist/firebaseui.css';
import { routes } from 'util/routes';

const authUi = new firebaseui.auth.AuthUI(firebase.auth);
const firebaseUiId = 'firebaseui-auth-container';

export class SignInScreen extends React.Component<{}, {}> {
  public componentDidMount() {
    authUi.start(`#${firebaseUiId}`, {
      signInOptions: [
        firebase.default.auth.EmailAuthProvider.PROVIDER_ID,
        firebase.default.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.default.auth.FacebookAuthProvider.PROVIDER_ID,
      ],
      tosUrl: routes.tos,
      signInSuccessUrl: routes.Home,
    });
  }

  public componentWillUnmount() {
    authUi.reset();
  }

  public render() {
    return <div id={firebaseUiId} />;
  }
}
