export const enum routes {
  Landing = '/',
  Home = '/resources',
  Login = '/login',
  tos = '/tos',
  Groups = '/groups',
  Settings = '/settings',
  Help = '/help',
}
