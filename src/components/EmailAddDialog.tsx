import * as React from 'react';
import Dialog, {
  DialogTitle,
  DialogActions,
  DialogContent,
  DialogContentText,
} from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import Button from 'material-ui/Button';
import * as EmailValidator from 'email-validator';

interface EmailAddDialogProps {
  isShowing: boolean;
  title: string;
  email?: string;
  handleNegativeClick: () => void;
  handlePositiveClick: (email: string) => void;
}

interface EmailAddDialogState {
  email: string;
  errors: {
    email?: string;
  };
}

export class EmailAddDialog extends React.Component<EmailAddDialogProps, EmailAddDialogState> {
  public state: EmailAddDialogState = {
    email: this.props.email ? this.props.email : '',
    errors: {},
  };

  public validate() {
    const errors: { email?: string } = {};
    if (!EmailValidator.validate(this.state.email)) {
      errors.email = 'Email must be valid';
    }
    this.setState({ errors });
    return Object.keys(errors).length === 0;
  }

  public hasErrors() {
    return Object.keys(this.state.errors).length > 0;
  }

  public onTextFieldChanged(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      email: e.target.value,
    });

    if (this.hasErrors()) {
      this.validate();
    }
  }

  public dismiss() {
    this.clear();
    this.props.handleNegativeClick();
  }

  public submit() {
    if (this.validate()) {
      this.props.handlePositiveClick(this.state.email);
      this.clear();
    }
  }

  public clear() {
    this.setState({
      email: '',
      errors: {},
    });
  }

  public render() {
    return (
      <Dialog open={this.props.isShowing} onClose={() => this.dismiss()}>
        <DialogTitle>{this.props.title}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To subscribe to this website, please enter your email address here. We will send updates
            occationally.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="email"
            label="Email"
            type="email"
            value={this.state.email}
            fullWidth
            required
            onChange={e => this.onTextFieldChanged(e)}
            error={this.hasErrors()}
            helperText={this.state.errors.email}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => this.dismiss()}>Cancel</Button>
          <Button onClick={() => this.submit()}>Add</Button>
        </DialogActions>
      </Dialog>
    );
  }
}
