import * as React from 'react';
import Button, { ButtonProps } from 'material-ui/Button';
import { ListItem, ListItemProps } from 'material-ui/List';
import { Link, LinkProps } from 'react-router-dom';

export const LinkButton: React.StatelessComponent<LinkProps & ButtonProps> = (
  props: LinkProps & ButtonProps
) => {
  const LinkAsButton = Link as React.ComponentType<ButtonProps>;
  return <Button component={LinkAsButton} {...props} />;
};

export const LinkListItem: React.StatelessComponent<LinkProps & ListItemProps> = (
  props: LinkProps & ListItemProps
) => {
  const LinkAsListItem = Link as React.ComponentType<ListItemProps>;
  return <ListItem button component={LinkAsListItem} {...props} />;
};
