import * as React from 'react';
import * as moment from 'moment';
import { inject, observer } from 'mobx-react';

import Stepper, { Step, StepLabel, StepContent, StepButton } from 'material-ui/Stepper';
import Typography from 'material-ui/Typography';
import Icon from 'material-ui/Icon';
import Button from 'material-ui/Button/Button';
import TimePicker from 'material-ui-pickers/TimePicker';
import { TimelineStep } from 'components/BookingTimeline';
import { BookingStore } from 'stores/bookings';
import { Booking } from 'models';

interface TimelineBookResourceProps {
  minSelectableTime: Date;
  maxSelectableTime: Date;
  selectedStartTime: Date;
  selectedEndTime: Date;
  setStartTime: (time: Date) => void;
  setEndTime: (time: Date) => void;
}

interface TimelineBookResourceInjectedProps {
  bookings?: BookingStore;
}

const TimelineBookResourceComponent: React.StatelessComponent<
  TimelineBookResourceProps & TimelineBookResourceInjectedProps
> = props => {
  const bookings = props.bookings as BookingStore;
  return (
    <div>
      <Typography variant="subheading">Available</Typography>
      <div style={{ marginBottom: 24 }}>
        <Typography variant="subheading">Start time</Typography>
        <TimePicker ampm={false} value={props.selectedStartTime} onChange={props.setStartTime} />
      </div>
      <div style={{ marginBottom: 24 }}>
        <Typography variant="subheading">End time</Typography>
        <TimePicker ampm={false} value={props.selectedEndTime} onChange={props.setEndTime} />
      </div>
      <Button
        variant="raised"
        onClick={() =>
          bookings.createBooking({
            id: '',
            resourceId: '',
            resource: '',
            renterId: '',
            resourceName: '',
            renterName: '',
            startTime: props.selectedStartTime,
            endTime: props.selectedEndTime,
            isApproved: false,
          })}
      >
        Book Now
      </Button>
    </div>
  );
};

export const TimelineBookResource = inject('bookings')(observer(TimelineBookResourceComponent));
