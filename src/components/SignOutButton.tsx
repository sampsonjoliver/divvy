import * as React from 'react';
import Button, { ButtonProps } from 'material-ui/Button';

import { auth } from 'wrappers/firebase';

export const SignOutButton = (props: ButtonProps) => {
  return (
    <Button {...props} onClick={() => auth.signOut()}>
      Log out
    </Button>
  );
};
