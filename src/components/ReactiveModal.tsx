import * as React from 'react';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
} from 'material-ui/Dialog';
import List, {
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  ListItemAvatar,
} from 'material-ui/List';
import Menu, { MenuItem } from 'material-ui/Menu';
import Radio from 'material-ui/Radio';
import Hidden from 'material-ui/Hidden';

interface ReactiveModalProps {
  isOpen: boolean;
  anchor?: HTMLElement & EventTarget;
  onModalItemSelected: (index: number) => void;
  items: ReactiveModalItems[];
  title: string;
  selectedItem: number;
}

interface ReactiveModalItems {
  text: string;
  secondaryText?: string;
}

export class ReactiveModal extends React.Component<ReactiveModalProps, {}> {
  public render() {
    const itemListViews = this.props.items.map((item, index) => {
      return (
        <ListItem key={index} button dense onClick={() => this.props.onModalItemSelected(index)}>
          <Radio checked={this.props.selectedItem === index} disableRipple />
          <ListItemText primary={item.text} secondary={item.secondaryText} />
        </ListItem>
      );
    });

    const itemMenuViews = this.props.items.map((item, index) => {
      return (
        <MenuItem
          key={index}
          selected={this.props.selectedItem === index}
          onClick={() => this.props.onModalItemSelected(index)}
        >
          {item.text}
        </MenuItem>
      );
    });

    return (
      <div>
        <Hidden mdUp>
          <Dialog open={this.props.isOpen} onClose={() => this.props.onModalItemSelected(-1)}>
            <DialogTitle>{this.props.title}</DialogTitle>
            <List>{itemListViews}</List>
          </Dialog>
        </Hidden>
        <Hidden smDown>
          <Menu
            open={this.props.isOpen}
            anchorEl={this.props.anchor}
            onClose={() => this.props.onModalItemSelected(-1)}
          >
            {itemMenuViews}
          </Menu>
        </Hidden>
      </div>
    );
  }
}
