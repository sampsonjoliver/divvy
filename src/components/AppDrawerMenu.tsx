import * as React from 'react';
import Icon from 'material-ui/Icon';
import List, {
  ListItem,
  ListItemAvatar,
  ListItemIcon,
  ListItemText,
  ListItemProps,
} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';
import { routes } from 'util/routes';
import { LinkListItem } from 'components/Links';
import { Link, LinkProps } from 'react-router-dom';

const styles: { [key: string]: React.CSSProperties } = {
  avatar: {
    margin: 10,
    color: '#fff',
    backgroundColor: '#18c',
  },
};

export const AppDrawerMenu = () => {
  return (
    <List style={{ display: 'flex', flexDirection: 'column', background: '#fff' }}>
      <LinkListItem to={routes.Settings}>
        <ListItemAvatar>
          <Avatar style={styles.avatar} alt="Your photo" />
        </ListItemAvatar>
        <ListItemText primary="You" />
      </LinkListItem>
      <LinkListItem to={routes.Home}>
        <ListItemIcon>
          <Icon>shopping_basket</Icon>
        </ListItemIcon>
        <ListItemText primary="Resources" />
      </LinkListItem>
      <LinkListItem to={routes.Groups}>
        <ListItemIcon>
          <Icon>group</Icon>
        </ListItemIcon>
        <ListItemText primary="Groups" />
      </LinkListItem>
      <Divider style={{ flex: 1, backgroundColor: '#ffffff' }} />
      <Divider />
      <LinkListItem to={routes.Settings}>
        <ListItemIcon>
          <Icon>settings</Icon>
        </ListItemIcon>
        <ListItemText primary="Settings" />
      </LinkListItem>
      <LinkListItem to={routes.Help}>
        <ListItemIcon>
          <Icon>help</Icon>
        </ListItemIcon>
        <ListItemText primary="Help" />
      </LinkListItem>
    </List>
  );
};
