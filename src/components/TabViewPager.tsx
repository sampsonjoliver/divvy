import SwipeableViews, { SwipeableViewsProps } from 'react-swipeable-views';
import * as React from 'react';
import { Theme } from 'material-ui/styles';

const styles: { [key: string]: React.CSSProperties } = {
  tabContent: {
    width: '100%',
    height: '100%',
  },
};

type TabViewPagerProps = SwipeableViewsProps & {
  theme: Theme;
};

export const TabViewPager = (props: TabViewPagerProps) => {
  const { theme, children, index, onChangeIndex } = props;
  return (
    <SwipeableViews
      index={index}
      onChangeIndex={onChangeIndex}
      containerStyle={styles.tabContent}
      style={styles.tabContent}
      axis={props.theme.direction === 'rtl' ? 'x-reverse' : 'x'}
    >
      {children}
    </SwipeableViews>
  );
};
