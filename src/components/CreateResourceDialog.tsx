import * as React from 'react';
import Dialog, {
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
} from 'material-ui/Dialog';
import List, {
  ListItem,
  ListItemText,
  ListItemAvatar,
  ListItemSecondaryAction,
} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Typography from 'material-ui/Typography';
import { ReactiveDialogForm, ReactiveDialogFormProps } from 'components/ReactiveDialogForm';
import { ReactiveModal } from 'components/ReactiveModal';
import { ApprovalPolicyOptions } from 'models/consts';
import { EmailAddDialog } from 'components/EmailAddDialog';

import AppBar from 'material-ui/AppBar';
import { AppToolBar } from 'components/AppToolBar';
import { Resource } from 'models';

interface CreateResourceDialogProps {
  isOpen: boolean;
  initialData?: Resource;
  handleNegativeClick: () => void;
  handlePositiveClick: (res: Resource) => void;
}

interface CreateResourceDialogState {
  approvalPolicyDialogIsOpen: boolean;
  addApproverDialogIsOpen: boolean;
  addRenterDialogIsOpen: boolean;
  approvalDialogAnchor?: EventTarget & HTMLElement;
  resource: Partial<Resource>;
  errors: { [key: string]: string };
}

class CreateResourceDialogComponent extends React.Component<
  CreateResourceDialogProps,
  CreateResourceDialogState
> {
  public state: CreateResourceDialogState = this.createBlankState();

  public createBlankState() {
    return {
      approvalPolicyDialogIsOpen: false,
      addApproverDialogIsOpen: false,
      addRenterDialogIsOpen: false,
      resource: {
        policy: ApprovalPolicyOptions[0].id,
        approvers: [],
        renters: [],
      },
      errors: {},
    };
  }

  public onAddApproversClicked() {
    this.setState({ addApproverDialogIsOpen: true });
  }

  public onAddRentersClicked() {
    this.setState({ addRenterDialogIsOpen: true });
  }

  public onAddApprover(approverEmail: string) {
    const approvers = this.state.resource.approvers ? this.state.resource.approvers : [];
    approvers.push(approverEmail);
    this.setState({
      addApproverDialogIsOpen: false,
      resource: {
        ...this.state.resource,
        approvers,
      },
    });
  }

  public onAddRenter(renterEmail: string) {
    const renters = this.state.resource.renters ? this.state.resource.renters : [];
    renters.push(renterEmail);
    this.setState({
      addRenterDialogIsOpen: false,
      resource: {
        ...this.state.resource,
        renters,
      },
    });
  }

  public onApprovalDialogItemSelected(selectedItem: number) {
    this.setState({
      approvalPolicyDialogIsOpen: false,
      resource: {
        ...this.state.resource,
        policy:
          ApprovalPolicyOptions[selectedItem >= 0 ? selectedItem : this.approvalPolicyIndex].id,
      },
    });
  }

  public onTextFieldChanged(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      resource: {
        ...this.state.resource,
        [e.target.id]: e.target.value,
      },
    });
    if (Object.keys(this.state.errors).length > 0) {
      this.validate(this.createResource());
    }
  }

  public onSubmit() {
    const resource = this.createResource();
    if (this.validate(resource)) {
      this.props.handlePositiveClick(resource);
      this.clear();
    }
  }

  public onDismiss() {
    this.props.handleNegativeClick();
    this.clear();
  }

  public clear() {
    this.setState(this.createBlankState());
  }

  public validate(resource: Resource): boolean {
    const errors: { [key: string]: string } = {};
    if (!resource.name || resource.name.length === 0) {
      errors.name = 'This is a required field';
    }
    if (!resource.description || resource.description.length === 0) {
      errors.description = 'This is a required field';
    }
    if (!resource.policy || resource.policy.length === 0) {
      errors.policy = 'This is a required field';
    }
    this.setState({ errors });
    if (Object.keys(errors).length > 0) {
      return false;
    } else {
      return true;
    }
  }

  public createResource(): Resource {
    return this.state.resource as Resource;
  }

  public removeApprover(index: number) {
    const approvers =
      this.state.resource.approvers &&
      this.state.resource.approvers.filter((value, i) => index !== i);
    this.setState({
      resource: {
        ...this.state.resource,
        approvers,
      },
    });
  }

  public removeRenter(index: number) {
    const renters =
      this.state.resource.renters && this.state.resource.renters.filter((value, i) => index !== i);
    this.setState({
      resource: {
        ...this.state.resource,
        renters,
      },
    });
  }

  get approvalPolicyIndex(): number {
    return ApprovalPolicyOptions.find(it => it.id === this.state.resource.policy)
      ? ApprovalPolicyOptions.findIndex(it => it.id === this.state.resource.policy)
      : 0;
  }

  public render() {
    const ApproversList =
      this.state.resource.approvers &&
      this.state.resource.approvers.map((approver, index) => {
        return (
          <ListItem button key={index}>
            <Avatar>
              <Icon>person</Icon>
            </Avatar>
            <ListItemText primary={approver} />
            <ListItemSecondaryAction>
              <IconButton onClick={() => this.removeApprover(index)}>close</IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        );
      });
    const RentersList =
      this.state.resource.renters &&
      this.state.resource.renters.map((renter, index) => {
        return (
          <ListItem button key={index}>
            <Avatar>
              <Icon>person</Icon>
            </Avatar>
            <ListItemText primary={renter} />
            <ListItemSecondaryAction>
              <IconButton onClick={() => this.removeRenter(index)}>close</IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        );
      });

    return (
      <div>
        <ReactiveDialogForm
          title="Create Resource"
          isOpen={this.props.isOpen}
          handleNegativeClick={() => this.onDismiss()}
          handlePositiveClick={() => this.onSubmit()}
        >
          <DialogTitle>Create Resource</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Let's create a new resource you can share with others!
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Name"
              type="text"
              value={this.state.resource.name}
              fullWidth
              required
              onChange={e => this.onTextFieldChanged(e)}
              error={this.state.errors.name !== undefined}
              helperText={this.state.errors.name}
            />
            <TextField
              margin="dense"
              id="description"
              label="Description"
              type="text"
              rows={2}
              value={this.state.resource.description}
              fullWidth
              required
              multiline
              onChange={e => this.onTextFieldChanged(e)}
              error={this.state.errors.description !== undefined}
              helperText={this.state.errors.description}
              style={{ marginBottom: 24 }}
            />
            <Typography variant="subheading">Policy</Typography>
            <ListItem
              button
              style={{ marginBottom: 24 }}
              onClick={event =>
                this.setState({
                  approvalPolicyDialogIsOpen: true,
                  approvalDialogAnchor: event.currentTarget,
                })}
            >
              <ListItemText
                primary={ApprovalPolicyOptions[this.approvalPolicyIndex].text}
                secondary={ApprovalPolicyOptions[this.approvalPolicyIndex].secondaryText}
              />
            </ListItem>

            <Typography variant="subheading">Approvers</Typography>
            <List>
              {ApproversList}
              <Button
                color="secondary"
                style={{ width: '100%' }}
                onClick={() => this.onAddApproversClicked()}
              >
                <Icon>add</Icon>
                Add Approver
              </Button>
            </List>

            <Typography variant="subheading">Renters</Typography>
            <List>
              {RentersList}
              <Button
                color="secondary"
                style={{ width: '100%' }}
                onClick={() => this.onAddRentersClicked()}
              >
                <Icon>add</Icon>
                Add Renters
              </Button>
            </List>
          </DialogContent>

          <ReactiveModal
            isOpen={this.state.approvalPolicyDialogIsOpen}
            onModalItemSelected={index => this.onApprovalDialogItemSelected(index)}
            anchor={this.state.approvalDialogAnchor}
            items={ApprovalPolicyOptions}
            title="Approval Policy"
            selectedItem={this.approvalPolicyIndex}
          />

          <EmailAddDialog
            isShowing={this.state.addApproverDialogIsOpen || this.state.addRenterDialogIsOpen}
            title={this.state.addApproverDialogIsOpen ? 'Add Approver' : 'Add Renter'}
            handleNegativeClick={() => {
              this.setState({
                addApproverDialogIsOpen: false,
                addRenterDialogIsOpen: false,
              });
            }}
            handlePositiveClick={(email: string) =>
              this.state.addApproverDialogIsOpen
                ? this.onAddApprover(email)
                : this.onAddRenter(email)}
          />
        </ReactiveDialogForm>
      </div>
    );
  }
}

export const CreateResourceDialog = withMobileDialog<CreateResourceDialogProps>({
  breakpoint: 'sm',
})(CreateResourceDialogComponent);
