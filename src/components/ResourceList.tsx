import * as React from 'react';
import { inject, observer } from 'mobx-react';
import List, { ListItem, ListItemText, ListItemIcon } from 'material-ui/List';
import MobxSocket from 'mobx-websocket-store';
import { ResourceStore } from 'stores/resources';
import { Resource } from 'models';

interface ResourceListInjectedProps {
  resources: ResourceStore;
}

@inject('resources')
@observer
export default class ResourceList extends React.Component<{}, {}> {
  get injected() {
    return this.props as ResourceListInjectedProps;
  }

  public render() {
    const {} = this.props;
    const { resources } = this.injected;
    const atomData = resources.data;
    let resourceViews: React.ReactChild | React.ReactChild[];
    if (atomData == null) {
      resourceViews = (
        <ListItem>
          <ListItemText primary="Fetching" />
        </ListItem>
      );
    } else if (atomData.length === 0) {
      resourceViews = (
        <ListItem>
          <ListItemText primary="No items" />
        </ListItem>
      );
    } else {
      resourceViews = atomData.map(item => (
        <ListItem button key={item.id}>
          <ListItemText primary={item.name} secondary={item.description} />
        </ListItem>
      ));
    }

    return (
      <div>
        <p>Going to try getting the things</p>
        <List>{resourceViews}</List>
      </div>
    );
  }
}
