import * as React from 'react';
import Hidden from 'material-ui/Hidden';
import Drawer, { DrawerProps } from 'material-ui/Drawer';
import SwipeableDrawer, { SwipeableDrawerProps } from 'material-ui/SwipeableDrawer';
import { Theme, withStyles, WithStyles } from 'material-ui/styles';

export const DrawerWidth = 250;
const decorate = withStyles(theme => ({
  root: {
    width: DrawerWidth,
    [theme.breakpoints.up('md')]: {
      position: 'relative',
      height: '100%',
    },
  },
}));

type Props = DrawerProps & SwipeableDrawerProps & { theme?: Theme };
export const AppDrawer = decorate<Props>(props => {
  const { classes, onClose, onOpen, open, children, ...otherProps } = props;
  return (
    <div style={{ height: '100%' }}>
      <Hidden mdUp>
        <SwipeableDrawer
          anchor={props.theme && props.theme.direction === 'rtl' ? 'right' : 'left'}
          open={open}
          onOpen={onOpen}
          onClose={onClose}
          classes={{
            paper: classes.root,
          }}
          {...otherProps}
        >
          {children}
        </SwipeableDrawer>
      </Hidden>
      <Hidden mdDown implementation="css">
        <Drawer
          variant="permanent"
          open
          style={{ height: '100%' }}
          classes={{
            paper: classes.root,
          }}
          {...otherProps}
        >
          {children}
        </Drawer>
      </Hidden>
    </div>
  );
});
