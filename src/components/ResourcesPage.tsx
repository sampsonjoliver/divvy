import * as React from 'react';
import { inject, observer } from 'mobx-react';
import { withTheme, Theme, WithStyles } from 'material-ui/styles';
import ResourceList from 'components/ResourceList';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import { Resource } from 'models';
import { ResourceStore } from 'stores';
import { CreateResourceDialog } from 'components/CreateResourceDialog';
import { Page } from 'components/Page';

interface ResourcesPageProps {
  theme: Theme;
}

type ResourcesPageInjectedProps = ResourcesPageProps & {
  resources: ResourceStore;
};

@inject('resources')
@observer
class ResourcesPageComponent extends React.Component<ResourcesPageProps, { dialogOpen: boolean }> {
  public state = {
    dialogOpen: false,
  };

  get injected() {
    return this.props as ResourcesPageInjectedProps;
  }

  public handleClickOpen = () => {
    this.setState({ dialogOpen: true });
  };

  public handleRequestClose = () => {
    this.setState({ dialogOpen: false });
  };

  public handleRequestSave = (obj: Resource) => {
    this.setState({ dialogOpen: false });
    this.injected.resources.createResource(obj);
  };

  public render() {
    return (
      <Page>
        <ResourceList />
        <Button
          variant="fab"
          color="secondary"
          style={{
            position: 'absolute',
            right: 0,
            bottom: 0,
            margin: this.props.theme.spacing.unit,
          }}
          onClick={this.handleClickOpen}
        >
          <Icon>add</Icon>
        </Button>
        <CreateResourceDialog
          isOpen={this.state.dialogOpen}
          handleNegativeClick={this.handleRequestClose}
          handlePositiveClick={this.handleRequestSave}
        />
      </Page>
    );
  }
}

export const ResourcesPage = withTheme()<{}>(ResourcesPageComponent);
