import * as React from 'react';
import { Provider as MobxProvider } from 'mobx-react';
import { Router } from 'components/Router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import MomentUtils from 'material-ui-pickers/utils/moment-utils';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';

import theme from 'globalStyles';
import { resourceStore, bookingStore, authStore } from 'stores';

export const Root: React.StatelessComponent<{}> = () => {
  return (
    <MuiThemeProvider theme={theme}>
      <MuiPickersUtilsProvider utils={MomentUtils}>
        <MobxProvider resources={resourceStore} auth={authStore} bookings={bookingStore}>
          <Router />
        </MobxProvider>
      </MuiPickersUtilsProvider>
    </MuiThemeProvider>
  );
};
