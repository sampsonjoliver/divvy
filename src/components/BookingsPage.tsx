import * as React from 'react';
import { withTheme, Theme } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Icon from 'material-ui/Icon';
import { Page } from 'components/Page';
import { CreateBookingDialog } from 'components/CreateBookingDialog';
import { Booking } from 'models';

interface BookingsPageProps {
  theme: Theme;
}

class BookingsPageComponent extends React.Component<BookingsPageProps, { dialogOpen: boolean }> {
  public state = {
    dialogOpen: false,
  };

  public handleClickOpen = () => {
    this.setState({ dialogOpen: true });
  };

  public handleRequestClose = () => {
    this.setState({ dialogOpen: false });
  };

  public handleRequestSave = (obj: Booking) => {
    this.setState({ dialogOpen: false });
  };

  public render() {
    return (
      <Page>
        <Button
          variant="fab"
          color="secondary"
          style={{
            position: 'absolute',
            right: 0,
            bottom: 0,
            margin: this.props.theme.spacing.unit,
          }}
          onClick={this.handleClickOpen}
        >
          <Icon>add</Icon>
        </Button>
        <CreateBookingDialog
          isOpen={this.state.dialogOpen}
          handleNegativeClick={this.handleRequestClose}
          handlePositiveClick={this.handleRequestSave}
        />
      </Page>
    );
  }
}

export const BookingsPage = withTheme()<{}>(BookingsPageComponent);
