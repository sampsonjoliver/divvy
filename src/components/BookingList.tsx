import * as React from 'react';
import { inject, observer } from 'mobx-react';
import MobxSocket from 'mobx-websocket-store';
import { BookingStore } from 'stores/bookings';
import { Booking } from 'models';
import List, {
  ListItem,
  ListItemText,
  ListItemAvatar,
  ListItemSecondaryAction,
} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import { CircularProgress } from 'material-ui/Progress';

interface BookingListProps {
  startRange: Date;
  endRange: Date;
}

interface BookingListInjectedProps extends BookingListProps {
  bookings: BookingStore;
}

@inject('bookings')
@observer
export default class BookingList extends React.Component<BookingListProps, {}> {
  get injected() {
    return this.props as BookingListInjectedProps;
  }

  public render() {
    const {} = this.props;
    const { bookings } = this.injected;
    const atomData = bookings.byDateRange(this.props.startRange, this.props.endRange).data;
    let resourceViews: React.ReactChild | React.ReactChild[];
    if (atomData == null) {
      resourceViews = <CircularProgress style={{ margin: 'auto', display: 'block' }} />;
    } else if (atomData.length === 0) {
      resourceViews = (
        <ListItem>
          <ListItemText primary="No items" />
        </ListItem>
      );
    } else {
      resourceViews = atomData.map(booking => (
        <ListItem key={booking.id}>
          <Avatar>
            <Icon>person</Icon>
          </Avatar>
          <ListItemText primary={booking.renterName} secondary={`${booking.startTime}`} />
          <ListItemSecondaryAction>
            <IconButton>close</IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      ));
    }

    return (
      <div>
        <p>Going to try getting the things</p>
        <List>{resourceViews}</List>
      </div>
    );
  }
}
