import * as React from 'react';
import { withTheme, Theme } from 'material-ui/styles';

interface PageProps {
  theme: Theme;
}

class PageComponent extends React.Component<PageProps, {}> {
  public render() {
    return (
      <div
        dir={this.props.theme && this.props.theme.direction}
        style={{ position: 'relative', height: '100%' }}
      >
        {this.props.children}
      </div>
    );
  }
}

export const Page = withTheme()<{}>(PageComponent);
