import * as React from 'react';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
} from 'material-ui/Dialog';
import Button from 'material-ui/Button';
import Hidden from 'material-ui/Hidden';
import AppBar from 'material-ui/AppBar';
import { AppToolBar } from 'components/AppToolBar';
import { WithWidthProps } from 'material-ui/utils/withWidth';

interface WithDialogProps {
  fullScreen: boolean;
}

export interface ReactiveDialogFormProps {
  title: string;
  isOpen: boolean;
  negativeButton?: string | null;
  positiveButton?: string | null;
  neutralButton?: string | null;
  handleNegativeClick?: () => void;
  handleNeutralClick?: () => void;
  handlePositiveClick?: () => void;
}

type ReactiveDialogFormInjectedProps<T> = WithDialogProps &
  WithWidthProps &
  ReactiveDialogFormProps;

class ReactiveDialogFormComponent<T> extends React.Component<ReactiveDialogFormProps, {}> {
  public static defaultProps: Partial<ReactiveDialogFormProps> = {
    negativeButton: 'Cancel',
    positiveButton: 'Save',
  };

  get injected() {
    return this.props as ReactiveDialogFormInjectedProps<T>;
  }

  public render() {
    return (
      <Dialog
        fullWidth
        disableBackdropClick
        disableEscapeKeyDown
        fullScreen={this.injected.fullScreen}
        open={this.props.isOpen}
        onClose={this.props.handleNegativeClick}
      >
        <Hidden mdUp>
          <AppBar>
            <AppToolBar
              title={this.props.title}
              upButtonType="cancel"
              onUpClicked={this.props.handleNegativeClick}
            >
              <Button color="primary" onClick={this.injected.handlePositiveClick}>
                save
              </Button>
            </AppToolBar>
          </AppBar>
        </Hidden>

        {this.props.children}

        <Hidden smDown>
          <DialogActions>
            {this.props.negativeButton &&
              this.props.handleNegativeClick && (
                <Button onClick={this.injected.handleNegativeClick} color="primary">
                  {this.props.negativeButton}
                </Button>
              )}

            {this.props.neutralButton &&
              this.props.handleNeutralClick && (
                <Button onClick={this.injected.handleNeutralClick} color="primary">
                  {this.props.neutralButton}
                </Button>
              )}

            {this.props.positiveButton &&
              this.props.handlePositiveClick && (
                <Button onClick={this.injected.handlePositiveClick} color="primary">
                  {this.props.positiveButton}
                </Button>
              )}
          </DialogActions>
        </Hidden>
      </Dialog>
    );
  }
}

export const ReactiveDialogForm = withMobileDialog<ReactiveDialogFormProps>({
  breakpoint: 'sm',
})(ReactiveDialogFormComponent);
