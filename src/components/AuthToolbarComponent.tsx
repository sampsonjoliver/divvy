import * as React from 'react';
import Button, { ButtonProps } from 'material-ui/Button';
import { inject, observer } from 'mobx-react';
import { Link, LinkProps } from 'react-router-dom';

import { auth } from 'wrappers/firebase';
import { LinkButton } from 'components/Links';
import { authStore, AuthStates, AuthStore } from 'stores/auth';
import { routes } from 'util/routes';

interface AuthToolbarComponentProps {
  auth?: AuthStore;
}

export const SignInButton = (props: ButtonProps) => {
  const typedProps = props as ButtonProps & LinkProps;
  return (
    <LinkButton {...typedProps} to="/login">
      Login
    </LinkButton>
  );
};

export const SignOutButton = (props: ButtonProps) => {
  return (
    <Button {...props} onClick={() => auth.signOut()}>
      Log out
    </Button>
  );
};

@inject('auth')
@observer
export class AuthToolbarComponent extends React.Component<AuthToolbarComponentProps, {}> {
  public render() {
    return this.props.auth && this.props.auth.authState === AuthStates.SIGNED_IN ? (
      <div>
        <LinkButton color="inherit" to={routes.Home}>
          Home
        </LinkButton>
        <SignOutButton color="inherit" />
      </div>
    ) : (
      <SignInButton color="inherit" />
    );
  }
}
