import * as React from 'react';
import * as moment from 'moment';

import Stepper, { Step, StepLabel, StepContent, StepButton } from 'material-ui/Stepper';
import Typography from 'material-ui/Typography';
import Icon from 'material-ui/Icon';
import { TimelineStep } from 'components/BookingTimeline';
import { TimelineBookResource } from 'components/TimelineBookResource';

interface TimelineAvailableNodeProps {
  stepIndex: number;
  isActive: boolean;
  step: TimelineStep;
  setActiveStep: (stepIndex: number) => void;
  children?: React.ReactNode;
}

export const TimelineAvailableNode: React.StatelessComponent<
  TimelineAvailableNodeProps
> = props => {
  return (
    <Step key={props.stepIndex} active={props.isActive} orientation='vertical'>
      <StepButton
        orientation="vertical"
        icon={<Icon color="primary">radio_button_unchecked</Icon>}
        style={{ justifyContent: 'flex-start' }}
        onClick={() => props.setActiveStep(props.stepIndex)}
      >
        <StepLabel>{moment(props.step.start).format('hh:mm a')}</StepLabel>
      </StepButton>
      <StepContent>{props.children}</StepContent>
    </Step>
  );
};
