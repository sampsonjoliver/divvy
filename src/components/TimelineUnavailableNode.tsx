import * as React from 'react';
import * as moment from 'moment';

import Stepper, { Step, StepLabel, StepContent, StepButton } from 'material-ui/Stepper';
import Typography from 'material-ui/Typography';
import Icon from 'material-ui/Icon';
import { TimelineStep } from 'components/BookingTimeline';

interface TimelineUnavailableNodeProps {
  stepIndex: number;
  isActive: boolean;
  step: TimelineStep;
  setActiveStep: (stepIndex: number) => void;
}

export const TimelineUnavailableNode: React.StatelessComponent<
  TimelineUnavailableNodeProps
> = props => {
  return (
    <Step key={props.stepIndex} active={props.isActive} orientation='vertical'>
      <StepButton
        orientation="vertical"
        icon={<Icon color="disabled">fiber_manual_record</Icon>}
        style={{ justifyContent: 'flex-start' }}
        onClick={() => props.setActiveStep(props.stepIndex)}
      >
        <StepLabel>{moment(props.step.start).format('hh:mma')}</StepLabel>
      </StepButton>
      <StepContent>
        <Typography variant="subheading">Booked out</Typography>
        <Typography variant="caption">
          Booked by {props.step.booking ? props.step.booking.renterName : 'unknown'}
        </Typography>
      </StepContent>
    </Step>
  );
};
