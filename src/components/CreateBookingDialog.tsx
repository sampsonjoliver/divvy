import { BookingTimeline } from './BookingTimeline';
import theme from '../globalStyles';
import register from '../registerServiceWorker';
import * as React from 'react';
import Dialog, {
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
  DialogActions,
} from 'material-ui/Dialog';
import List, {
  ListItem,
  ListItemText,
  ListItemAvatar,
  ListItemSecondaryAction,
} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Typography from 'material-ui/Typography';
import { ReactiveDialogForm, ReactiveDialogFormProps } from 'components/ReactiveDialogForm';
import { ReactiveModal } from 'components/ReactiveModal';
import Calendar from 'react-calendar-material';
import Divider from 'material-ui/Divider';
import * as moment from 'moment';
import Stepper, { Step, StepLabel } from 'material-ui/Stepper';
import SwipeableViews from 'react-swipeable-views';

import AppBar from 'material-ui/AppBar';
import { AppToolBar } from 'components/AppToolBar';
import { Booking } from 'models';
import { withTheme, Theme } from 'material-ui/styles';
import StepContent from 'material-ui/Stepper/StepContent';

interface CreateBookingDialogProps {
  isOpen: boolean;
  initialData?: Booking;
  handleNegativeClick: () => void;
  handlePositiveClick: (res: Booking) => void;
}

interface CreateBookingDialogThemedProps extends CreateBookingDialogProps {
  theme: Theme;
}

interface CreateBookingDialogState {
  selectedDateStartTime: Date;
  selectedDateEndTime: Date;
  currentBooking: Booking;
  isTimePickerOpen: boolean;
  pendingTime: number;
}

class CreateBookingDialogComponent extends React.Component<
  CreateBookingDialogThemedProps,
  CreateBookingDialogState
> {
  public state: CreateBookingDialogState = this.createBlankState();

  public createBlankState(): CreateBookingDialogState {
    return {
      selectedDateStartTime: moment()
        .startOf('day')
        .toDate(),
      selectedDateEndTime: moment()
        .endOf('day')
        .toDate(),
      isTimePickerOpen: false,
      pendingTime: 0,
      currentBooking: this.emptyBooking(),
    };
  }

  public emptyBooking(): Booking {
    return {
      id: '',
      resource: '',
      resourceId: '',
      renterId: '',
      renterName: '',
      resourceName: '',
      startTime: new Date(),
      endTime: new Date(),
      isApproved: false,
    };
  }

  public onDismiss() {
    this.props.handleNegativeClick();
    this.setState(this.createBlankState());
  }

  public onSubmit() {
    this.props.handlePositiveClick(this.emptyBooking());
    this.setState(this.createBlankState());
  }

  public render() {
    return (
      <ReactiveDialogForm
        title="Book Resource"
        isOpen={this.props.isOpen}
        positiveButton="Book"
        negativeButton="Cancel"
        handleNegativeClick={() => this.onDismiss()}
        handlePositiveClick={() => this.onSubmit()}
      >
        <DialogTitle>Book Resource</DialogTitle>
        <DialogContent>
          <DialogContentText>Let's make that booking!</DialogContentText>
          <Calendar
            accentColor={this.props.theme.palette.primary.main}
            orientation="flex-col"
            showHeader={false}
            onDatePicked={date => {
              this.setState({
                selectedDateStartTime: moment(date)
                  .startOf('day')
                  .toDate(),
                selectedDateEndTime: moment(date)
                  .endOf('day')
                  .toDate(),
              });
            }}
          />
          <Divider />

          <Typography variant="subheading">Bookings:</Typography>
          <BookingTimeline
            startRange={this.state.selectedDateStartTime}
            endRange={this.state.selectedDateEndTime}
          />
        </DialogContent>
      </ReactiveDialogForm>
    );
  }
}

const CreateBookingThemedDialog = withTheme()<CreateBookingDialogProps>(
  CreateBookingDialogComponent
);

export const CreateBookingDialog = withMobileDialog<CreateBookingDialogProps>({
  breakpoint: 'sm',
})(CreateBookingThemedDialog);
