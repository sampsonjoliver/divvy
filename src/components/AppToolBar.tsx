import * as React from 'react';
import Toolbar, { ToolbarProps } from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import Tabs, { Tab, TabsProps } from 'material-ui/Tabs';
import { withStyles, Theme, WithStyles } from 'material-ui/styles';

const decorate = withStyles(theme => ({
  upIcon: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));

type AppToolbarProps = ToolbarProps & {
  title: string;
  upButtonType?: 'menu' | 'back' | 'cancel' | 'none';
  onUpClicked?: () => void;
};

export const AppToolBar = decorate<AppToolbarProps>(props => {
  const { classes, children, title, upButtonType, onUpClicked, ...toolbarProps } = props;
  const upIconName = upButtonTypeToIconName(upButtonType);
  return (
    <Toolbar {...toolbarProps}>
      <IconButton
        className={upButtonType === 'menu' ? classes.upIcon : ''}
        onClick={props.onUpClicked}
        color="secondary"
        aria-label="Menu"
      >
        <Icon>{upIconName}</Icon>
      </IconButton>
      <Typography variant="title" color="inherit" style={{ flex: 1 }}>
        {props.title}
      </Typography>
      {props.children}
    </Toolbar>
  );
});

function upButtonTypeToIconName(upButtonType?: string) {
  switch (upButtonType) {
    case 'menu':
      return 'menu';
    case 'back':
      return 'arrow_back';
    case 'cancel':
      return 'clear';
    default:
      return null;
  }
}

type AppTabBarProps = TabsProps;

export const AppTabBar: React.StatelessComponent<AppTabBarProps> = (props: AppTabBarProps) => {
  const { children, ...otherProps } = props;
  return (
    <Tabs {...otherProps} fullWidth={true} centered={true}>
      {children}
    </Tabs>
  );
};
