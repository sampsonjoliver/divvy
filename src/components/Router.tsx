import * as React from 'react';
import { observer, inject } from 'mobx-react';
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect,
  withRouter,
  RouteComponentProps,
} from 'react-router-dom';
import * as Screens from 'screens';
import { AuthStore, AuthStates } from 'stores/auth';
import { routes } from 'util/routes';

// tslint:disable-next-line
type RedirectProps = RouteComponentProps<any> & {
  authState: AuthStates;
};

const AuthRedirect: React.StatelessComponent<RedirectProps> = (props: RedirectProps) => {
  if (
    props.authState === AuthStates.SIGNED_OUT &&
    (props.location.pathname !== routes.Login && props.location.pathname !== routes.Landing)
  ) {
    return <Redirect to={routes.Landing} />;
  } else if (props.authState === AuthStates.SIGNED_IN && props.location.pathname === routes.Login) {
    return <Redirect to={routes.Home} />;
  } else {
    return null;
  }
};
const AuthRedirectRouter = withRouter(AuthRedirect);

interface RouterProps {
  auth?: AuthStore;
}

export const RouterComponent: React.StatelessComponent<RouterProps> = (props: RouterProps) => {
  const auth = props.auth as AuthStore;
  const authState = auth.authState;
  return (
    <BrowserRouter>
      <div>
        <AuthRedirectRouter authState={authState} />
        <Switch>
          <Route path={routes.Login} component={Screens.SignInScreen} />
          <Route path={routes.Home} component={Screens.HomeScreen} />
          <Route path={routes.Groups} component={Screens.GroupScreen} />
          <Route path={routes.Settings} component={Screens.SettingsScreen} />
          <Route path={routes.Help} component={Screens.HelpScreen} />
          <Route exact path={routes.Landing} component={Screens.LandingScreen} />
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export const Router = inject('auth')(observer(RouterComponent));
