import * as React from 'react';
import { inject, observer } from 'mobx-react';
import MobxSocket from 'mobx-websocket-store';
import { BookingStore } from 'stores/bookings';
import { Booking } from 'models';
import List, {
  ListItem,
  ListItemText,
  ListItemAvatar,
  ListItemSecondaryAction,
} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import Icon from 'material-ui/Icon';
import { CircularProgress } from 'material-ui/Progress';
import Stepper, { Step, StepLabel, StepContent, StepButton } from 'material-ui/Stepper';
import Typography from 'material-ui/Typography';
import * as moment from 'moment';
import Button from 'material-ui/Button/Button';
import { Page } from 'components/Page';
import { TimelineUnavailableNode } from 'components/TimelineUnavailableNode';
import { TimelineAvailableNode } from 'components/TimelineAvailableNode';
import { TimelineBookResource } from 'components/TimelineBookResource';

export interface TimelineStep {
  start: Date;
  end: Date;
  booking?: Booking;
}

interface BookingTimelineProps {
  startRange: Date;
  endRange: Date;
}

interface BookingTimelineInjectedProps extends BookingTimelineProps {
  bookings: BookingStore;
}

interface BookingTimelineState {
  activeStep: number;
  selectedStartDates: { [index: number]: Date };
  selectedEndDates: { [index: number]: Date };
}

@inject('bookings')
@observer
export class BookingTimeline extends React.Component<BookingTimelineProps, BookingTimelineState> {
  public state: BookingTimelineState = this.createBlankState();

  get injected() {
    return this.props as BookingTimelineInjectedProps;
  }

  public createBlankState() {
    return {
      activeStep: -1,
      selectedStartDates: [],
      selectedEndDates: [],
    };
  }

  public setActiveStep(index: number) {
    this.setState({ activeStep: index });
  }

  public renderBookedNode(step: TimelineStep, isActive: boolean, stepIndex: number) {
    return (
      <TimelineUnavailableNode
        key={stepIndex}
        stepIndex={stepIndex}
        isActive
        step={step}
        setActiveStep={() => this.setActiveStep(stepIndex)}
      />
    );
  }

  public renderAvailableNode(step: TimelineStep, isActive: boolean, stepIndex: number) {
    return (
      <TimelineAvailableNode
        key={stepIndex}
        stepIndex={stepIndex}
        isActive={isActive}
        step={step}
        setActiveStep={() => this.setActiveStep(stepIndex)}
      >
        <TimelineBookResource
          minSelectableTime={step.start}
          maxSelectableTime={step.end}
          selectedStartTime={
            this.state.selectedStartDates[stepIndex]
              ? this.state.selectedStartDates[stepIndex]
              : step.start
          }
          selectedEndTime={
            this.state.selectedEndDates[stepIndex]
              ? this.state.selectedEndDates[stepIndex]
              : step.end
          }
          setStartTime={time =>
            this.setState({
              selectedStartDates: {
                ...this.state.selectedStartDates,
                [stepIndex]: time,
              },
            })}
          setEndTime={time =>
            this.setState({
              selectedEndDates: {
                ...this.state.selectedStartDates,
                [stepIndex]: time,
              },
            })}
        />
      </TimelineAvailableNode>
    );
  }

  public renderBookings(bookings: Booking[]) {
    const bookingNodes: React.ReactNode[] = [];
    const startOfDay = moment().startOf('day');
    const endOfDay = moment().endOf('day');

    const timeline: TimelineStep[] = [];
    bookings.forEach((booking, index) => {
      if (timeline.length > 0) {
        const lastTimelineEntry = timeline[timeline.length - 1];
        if (lastTimelineEntry.booking && lastTimelineEntry.end < booking.startTime) {
          const freeSpace = {
            start: lastTimelineEntry.end,
            end: booking.startTime,
          };
          timeline.push(freeSpace);
        }
      }

      if (index === 0 && booking.startTime > startOfDay.toDate()) {
        const freeSpace = {
          start: startOfDay.toDate(),
          end: booking.startTime,
        };
        const bookedSpace = {
          start: booking.startTime,
          end: booking.endTime,
          booking,
        };
        timeline.push(freeSpace);
        timeline.push(bookedSpace);
      } else if (index === bookings.length - 1 && booking.endTime < endOfDay.toDate()) {
        const bookedSpace = {
          start: booking.startTime,
          end: booking.endTime,
          booking,
        };
        const freeSpace = {
          start: booking.endTime,
          end: new Date(endOfDay.toDate().getTime() - booking.endTime.getTime()),
        };
        const endNode = {
          start: endOfDay.toDate(),
          end: endOfDay.toDate(),
        };
        timeline.push(bookedSpace);
        timeline.push(freeSpace);
        timeline.push(endNode);
      } else {
        const bookedSpace = {
          start: booking.startTime,
          end: booking.endTime,
          booking,
        };
        timeline.push(bookedSpace);
      }
    });

    return timeline.map((entry, index) => {
      if (entry.booking) {
        return this.renderBookedNode(entry, this.state.activeStep === index, index);
      } else {
        return this.renderAvailableNode(entry, this.state.activeStep === index, index);
      }
    });
  }

  public render() {
    const {} = this.props;
    const { bookings } = this.injected;
    const atomData = bookings.byDateRange(this.props.startRange, this.props.endRange).data;
    let resourceViews: React.ReactChild | React.ReactChild[];
    if (atomData == null) {
      resourceViews = <CircularProgress style={{ margin: 'auto', display: 'block' }} />;
    } else if (atomData.length === 0) {
      resourceViews = (
        <Stepper orientation="vertical" activeStep={0} nonLinear>
          {this.renderAvailableNode(
            {
              start: moment()
                .startOf('day')
                .toDate(),
              end: moment()
                .endOf('day')
                .toDate(),
            },
            true,
            0
          )}
        </Stepper>
      );
    } else {
      resourceViews = (
        <Stepper orientation="vertical" activeStep={this.state.activeStep} nonLinear>
          {this.renderBookings(atomData)}
        </Stepper>
      );
    }

    return resourceViews;
  }
}
