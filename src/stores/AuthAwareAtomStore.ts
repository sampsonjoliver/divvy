import * as Firebase from 'firebase';
import MobxSocket from 'mobx-websocket-store';
import { authStore } from 'stores/auth';
import { action, autorun, observable } from 'mobx';
import { WithId } from 'models';
import { NamedStore } from 'stores/NamedStore';

export abstract class AuthAwareAtomStore<T extends WithId> extends NamedStore {
  public _name: string = 'AuthAwareAtomStore';
  public uid: string = '';
  public query?: Firebase.firestore.Query;
  @observable public resourceAtom?: MobxSocket<T[]>;
  public unsubscribe: (() => void) | null = null;

  get data() {
    if (this.resourceAtom) {
      return this.resourceAtom.data;
    } else {
      return this.resourceAtom;
    }
  }

  get name(): string {
    return this._name;
  }

  public abstract getQuery(): Firebase.firestore.Query;

  constructor(name: string) {
    super();
    this._name = name;

    autorun('AuthAwareStore', () => {
      const user = authStore.user;
      if (user) {
        this.log('Auth store user changed. Changing snapshot to uid: ' + user.uid);
        this.registerUid(user.uid);
      }
    });
  }

  public startListening(this: AuthAwareAtomStore<T>, socket: MobxSocket<T[]>) {
    if (this.unsubscribe) {
      this.log(`Closing resources store for stale connection`);
      this.unsubscribe();
    }

    this.log(`Opening resources store for ${this.uid}`);
    if (this.query) {
      const dispose = this.query.onSnapshot(
        snapshot => {
          this.log(`Got a snapshot for ${this.uid}`);
          const data: T[] = snapshot.docs.map(doc => {
            const res = doc.data() as T;
            res.id = doc.id;
            return res;
          });
          socket.data = data;
          socket.atom.reportChanged();
        },
        err => {
          this.log('Something went wrong.', err);
        }
      );

      this.unsubscribe = () => {
        dispose();
        this.unsubscribe = null;
      };
    }
  }

  public stopListening(this: AuthAwareAtomStore<T>, socket: MobxSocket<T[]>) {
    if (socket === this.resourceAtom && this.unsubscribe) {
      this.log(`Closing resources store for ${this.uid}`);
      this.unsubscribe();
    }
  }

  @action
  public registerUid(uid: string) {
    if (this.resourceAtom && this.resourceAtom.atom.isBeingTracked) {
      this.resourceAtom.stopListening();
    }

    this.uid = uid;
    this.query = this.getQuery();
    this.resourceAtom = new MobxSocket<T[]>(
      this.startListening.bind(this),
      this.stopListening.bind(this)
    );
  }
}
