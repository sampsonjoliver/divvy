import firebase from '../wrappers/firebase';
import * as Firebase from 'firebase';
import MobxSocket from 'mobx-websocket-store';
import { authStore } from 'stores/auth';
import { action, autorun, observable } from 'mobx';
import { WithId } from 'models';
import { NamedStore } from 'stores/NamedStore';

interface Store<T> {
  socket: MobxSocket<T[]>;
  query: Firebase.firestore.Query;
  unsub?: () => void;
}

export abstract class StoreFactory<T extends WithId> extends NamedStore {
  public _name: string = 'StoreFactory';
  private stores: Map<string, Store<T>>;

  get name(): string {
    return this._name;
  }

  constructor(name: string) {
    super();
    this._name = name;
    this.stores = new Map();
  }

  public abstract getCollection(): Firebase.firestore.CollectionReference;

  public getOrCreateStore(key: string, query: firebase.firestore.Query): Store<T> {
    if (!this.stores.has(key)) {
      this.stores.set(key, {
        socket: new MobxSocket(this.startListening.bind(this), this.stopListening.bind(this), {
          id: key,
          resetDataOnOpen: false,
        }),
        query,
      });
    }

    const store = this.stores.get(key) as Store<T>;
    return store;
  }

  public startListening(this: StoreFactory<T>, socket: MobxSocket<T[]>) {
    const key = socket.id;
    const store = this.stores.get(key) as Store<T>;
    this.log(`Opening resources store for ${key}`);

    const dispose = store.query.onSnapshot(
      snapshot => {
        this.log(`Got a snapshot for ${key}`);
        const data: T[] = snapshot.docs.map(doc => {
          const res = doc.data() as T;
          res.id = doc.id;
          return res;
        });
        socket.data = data;
        socket.atom.reportChanged();
      },
      err => {
        this.log('Something went wrong.', err);
      }
    );

    store.unsub = () => {
      dispose();
      store.unsub = undefined;
    };
  }

  public stopListening(this: StoreFactory<T>, socket: MobxSocket<T[]>) {
    const key = socket.id;
    if (this.stores.has(key)) {
      this.log(`Closing resources store for ${key}`);
      const store = this.stores.get(key) as Store<T>;
      if (store.unsub) {
        store.unsub();
      }
    }
  }
}
