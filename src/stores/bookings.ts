import firebase from '../wrappers/firebase';
import { StoreFactory } from './StoreFactory';
import { Page } from '../components/Page';
import { action } from 'mobx';
import { firestore } from 'wrappers/firebase';
import { Booking } from 'models';
import { AuthAwareAtomStore } from 'stores/AuthAwareAtomStore';
import MobxSocket from 'mobx-websocket-store';
import { Socket } from 'net';

export class BookingStore extends StoreFactory<Booking> {
  constructor() {
    super('BookingStore');
  }

  public getCollection(): firebase.firestore.CollectionReference {
    return firestore.collection('bookings');
  }

  public byResourceId(resourceId: string): MobxSocket<Booking[]> {
    const query = this.getCollection().where('', '==', resourceId);
    return this.getOrCreateStore(resourceId, query).socket;
  }

  public byDateRange(start: Date, end: Date): MobxSocket<Booking[]> {
    const query = this.getCollection()
      .where('startTime', '>=', start)
      .where('startTime', '<=', end);
    return this.getOrCreateStore(`${start.toString()}-${end.toString()}`, query).socket;
  }

  @action
  public createBooking(booking: Booking) {
    this.getCollection().add(booking);
  }
}

export const bookingStore = new BookingStore();
