export abstract class NamedStore {
  abstract get name(): string;

  protected log(message: string, ...optionalParams: Array<{}>) {
    if (optionalParams && optionalParams.length > 0) {
      console.debug(`${this.name}: ${message}`, optionalParams);
    } else {
      console.debug(`${this.name}: ${message}`);
    }
  }
}
