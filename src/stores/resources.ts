import { action } from 'mobx';
import { firestore } from 'wrappers/firebase';
import { Resource } from 'models';
import { AuthAwareAtomStore } from 'stores/AuthAwareAtomStore';

export class ResourceStore extends AuthAwareAtomStore<Resource> {
  constructor() {
    super('ResourceStore');
  }

  public getQuery() {
    return firestore.collection('resources').where('owner', '==', this.uid);
  }

  @action
  public createResource(resource: Resource) {
    resource.owner = this.uid;
    firestore.collection('resources').add(resource);
  }
}

export const resourceStore = new ResourceStore();
