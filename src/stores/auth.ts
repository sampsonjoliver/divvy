import { observable } from 'mobx';
import firebase, { auth } from 'wrappers/firebase';

export const enum AuthStates {
  SIGNED_OUT = 'SIGNED_OUT',
  SIGNED_IN = 'SIGNED_IN',
  PENDING = 'PENDING',
}

export class AuthStore {
  @observable public authState: AuthStates;
  @observable public user: firebase.User | null = null;

  constructor() {
    this.authState = AuthStates.PENDING;
    auth.onAuthStateChanged(providerUser => {
      console.debug('authstatechanged: ', providerUser);
      if (providerUser) {
        this.authState = AuthStates.SIGNED_IN;
        this.user = providerUser;
      } else {
        this.authState = AuthStates.SIGNED_OUT;
        this.user = null;
      }
    });
  }
}

export const authStore = new AuthStore();
