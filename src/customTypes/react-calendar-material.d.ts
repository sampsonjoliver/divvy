/// <reference types="react" />

declare module 'react-calendar-material' {
  interface CalendarProps {
    accentColor?: string;
    orientation?: 'flex-row' | 'flex-col';
    showHeader?: boolean;
    onDatePicked?: (date: Date) => void;
  }

  const Calendar: React.ComponentType<CalendarProps>;

  export default Calendar;
}
