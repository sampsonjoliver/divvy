/// <reference types="react" />

declare module 'material-ui-time-picker' {
  interface TimeInputProps {
    mode: '12h' | '24h';
    onChange?: (time: Date) => void;
    value?: Date;
    autoOk?: boolean;
    cancelLabel?: string;
    defaultValue?: number;
    okLabel?: string;
    fullWidth?: boolean;
  }

  const TimeInput: React.ComponentType<TimeInputProps>;

  export default TimeInput;
}
