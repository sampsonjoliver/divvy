/// <reference types="firebase" />

declare module 'firebaseui' {
  import * as firebase from 'firebase';
  var out: FirebaseUiExport;
  export default out;

  interface FirebaseUiExport {
    auth: auth;
  }

  interface auth {
    AuthUI: typeof auth.AuthUI;
  }

  namespace auth {
    type UiConfig = {
      callbacks?: Callbacks;
      queryParameterForSignInSuccessUrl?: string;
      queryParameterForWidgetMode?: string;
      signInFlow?: 'redirect' | 'popup';
      signInOptions: Array<SignInOption | string> | string[];
      signInSuccessUrl?: string;
      tosUrl: string;
    };

    type SignInOption = {
      provider: string;
      scopes?: Array<string>;
      requireDisplayName?: boolean;
    };

    interface Callbacks {
      signInSuccess?: (
        currentUser: firebase.User,
        credential?: firebase.auth.AuthCredential,
        redirectUrl?: string
      ) => boolean;
      uiShown?: () => void;
    }

    class AuthUI {
      constructor(auth: firebase.auth.Auth);
      start(elementId: string, uiConfig: UiConfig): void;
      reset(): void;
    }
  }
}
