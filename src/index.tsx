import * as React from 'react';
import * as ReactDOM from 'react-dom';
import registerServiceWorker from 'registerServiceWorker';
import { Root } from 'components/Root';

ReactDOM.render(
  <Root>
    <div className="App">
      <div className="App-header">
        <h2>Welcome to React</h2>
      </div>
      <p className="App-intro">
        To get started, edit <code>src/App.tsx</code> and save to reload.
      </p>
    </div>
  </Root>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
