export const ApprovalPolicyOptions = [
  {
    id: 'ffa',
    text: 'Free for all',
    secondaryText: "No approval is needed; anyone can nab it whenever it's free",
  },
  {
    id: 'maj',
    text: 'Majority',
    secondaryText: 'A majority of all approvers must sign off before this can be approved',
  },
  {
    id: 'all',
    text: 'All or none',
    secondaryText: 'Every approver must sign off before this can be approved',
  },
  {
    id: 'one',
    text: 'Just one',
    secondaryText: 'Only one approval must sign off before this can be approved',
  },
];
