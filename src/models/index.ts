import { intercept } from 'mobx/lib/api/intercept';
import { firestore } from 'firebase';

interface WithId {
  id: string;
}

interface Resource extends WithId {
  name: string;
  description: string;
  owner: string;
  approvers: string[];
  renters: string[];
  photos: string[];
  policy: string;
}

interface Booking extends WithId {
  resourceId: string;
  resource: string | firestore.DocumentReference;
  renterId: string;
  resourceName: string;
  renterName: string;
  startTime: Date;
  endTime: Date;
  isApproved: boolean;
}

export { Resource, Booking, WithId };
